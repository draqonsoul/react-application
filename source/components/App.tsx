import React from "react";
import {Headline, Numberbox, Switch} from "draqon-library";

const App = () => {
    
    return (
        <div>
            <h1>Webpack Boilerplate</h1>
            <Switch value={true} />
            <Numberbox value={2} min={0} max={100} />
            <Headline size="large">yo</Headline>
        </div>
    )
}

export default App;